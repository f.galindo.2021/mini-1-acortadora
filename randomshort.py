import string
import random
import webapp
import urllib.parse
import shelve

FORM = """
    <hr>
    <form action="/" method="post">
      <div>
        <label>Url a acortar: </label>
        <textarea name="content" rows="5" cols="33" required></textarea>
      </div>
      <div>
        <input type="submit" value="Submit">
      </div>
    </form>
"""

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div>
      <p>ACORTADOR DE URLs</p>
    </div>
    <div>
      {form}
    </div>
    <div>
        <p>Url list:</p>
        {dictionary_items}
    </div>
  </body>
</html>

"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div>
      <p>Resource not found: {resource}.</p>
    </div>
    <div>
      {form}
    </div>
  </body>
</html>
"""


def random_url():
    url = ''.join(random.choices(string.ascii_letters + string.digits, k=12))
    url = '/' + url
    return url


class ShortUrl(webapp.webApp):
    contents = shelve.open("contents")
    contents['/'] = PAGE.format(resource='/', form=FORM, dictionary_items='')

    def __init__(self, host, port):
        super().__init__(host, port)

    def url_list(self):
        items_html = ""
        for key, value in self.contents.items():
            if key == '/':
                continue
            items_html += "<p>{}: {}</p>".format(key, value)
        return items_html

    # Método para parsear la solicitud recibida y extraer las palabras clave

    def parse(self, request):
        data = {}
        params = request.split()
        data['method'] = params[0]
        print("method", data['method'])
        data['resource'] = params[1]
        print("resource", data['resource'])
        if data['method'] == "POST":
            body = params[-1].split('=', 1)
            if len(body) > 1:
                data['body'] = urllib.parse.unquote(body[1])
                if data['body'].find("https://") == -1 and data['body'].find("http://") == -1:
                    data['body'] = "https://" + data['body']
            else:
                data['body'] = ""
        return data

    def process(self, data):
        if data['method'] == "GET":
            page, code = self.get(data['resource'])
        elif data['method'] == "POST":
            page, code = self.post(data['resource'], data['body'])
        return (code, page)

    def get(self, resource):
        if resource in self.contents:
            if resource == '/':
                page = PAGE.format(resource=resource, form=FORM,
                                   dictionary_items=self.url_list())  # Actualizar la página con la lista actualizada
                # de URLs
                code = "200 OK"
            else:
                code = "302 Found"
                page = "HTTP/1.1" + " " + code + "\r\n"
                page += "Location:" + self.contents[resource] + "\r\n\r\n"

        else:
            page = PAGE_NOT_FOUND
            code = "404 Resource Not Found"
        return page, code

    def post(self, resource, body):
        if resource in self.contents:
            if resource == '/' and len(body) > 0:
                r_url = random_url()
                self.contents[r_url] = body
                page = PAGE.format(resource=resource, form=FORM,
                                   dictionary_items=self.url_list())  # Actualizar la página con la lista actualizada
                # de URLs
                code = "200 OK"
            else:
                page = PAGE_NOT_FOUND
                code = "404 Resource Not Found"
        else:
            page = PAGE_NOT_FOUND
            code = "404 Resource Not Found"
        return page, code


if __name__ == '__main__':
    shorturl = ShortUrl("localhost", 1234)
